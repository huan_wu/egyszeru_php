<?php

include 'connect.php';

$sql = "SELECT * FROM MyTable";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo $row["id"] . ". " . $row["firstname"] . " " . $row["lastname"] . ", " . $row["email"] . ", " . $row["reg_date"] . "<br>";
    } 
} else {
    echo "0 results";
}

$conn->close();

?>
